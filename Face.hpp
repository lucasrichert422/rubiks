class Face{
	char* face;

public:
	Face(char couleur);
	Face();
	~Face();
	void Rotation();
	char* getCol(int pos);
	char* getLine(int pos);
	char* changeLine(char* line, int pos);
	char* changeCol(char* col, int pos);
	char* getFace();
	void afficherFace();
};
