#include "Cube.hpp"
#include <iostream>
#include <cstring>
#include <cstdlib>

Cube::Cube(){
  char colorList[6] = {'r','b','o','g','w','y'}; // ordre des couleurs dans la liste des faces
  this->listeFace = new Face[6];
  for(int i=0; i<6; ++i){
    this->listeFace[i] = Face(colorList[i]); 
  }
  this->frontFace = 0;
  this->topFace = 4;
}

Cube::~Cube(){

}

int Cube::getIntFace(char face){
  switch (face){
    case 'U': 
      return this->topFace;
      break;
    case 'F':
      return this->frontFace;
      break;
    case 'R':
      return (this->frontFace+1) %4;
      break;
    case 'L':
     return (this->frontFace+3) %4;
      break;
    case 'B':
      return (this->frontFace+2) %4;
      break;
    case 'D':
      return ((this->topFace-3) %2)+4;
      break;
    default:
      std::cout<<"this face doesn't exist"<<std::endl;
      return -1;
  }

}

void Cube::rotation(int face){ // rotation d'un face et changement des ligne et colonnes autour
  char* temp = new char[3];
  this->listeFace[face].Rotation();
  if(face<4){ // faces de la bague
  	temp =this->listeFace[(face-1+4)%4].getCol(2); // get la colonne pour remplacer la suivante
  	if(face%2==0){ 
  		temp = this->listeFace[4].changeCol(temp,((face+3)%4)-1);
      temp = this->listeFace[(face+1)%4].changeCol(temp,0); // change la colonne avec celle recup avant
      temp = this->listeFace[5].changeCol(temp,((face+3)%4)-1);
  	}
  	else{
      temp = this->listeFace[4].changeLine(temp,face-1);
      temp = this->listeFace[(face+1)%4].changeCol(temp,0); // change la colonne avec celle recup avant
      temp = this->listeFace[5].changeLine(temp,face-1);
  	}
    this->listeFace[(face-1+4)%4].changeCol(temp,2); // changement de la premiere colonne recupérée
  }
  if(face==WHITE){ // rotation autour de la face blanche (4)
    temp = this->listeFace[0].getLine(0);
  	for(int i=3; i>=0;--i){
  		temp = this->listeFace[i].changeLine(temp,0);
  	}
  }
  if(face==YELLOW){ //rotation autour de la face jaune (5)
    temp = this->listeFace[3].getLine(2);
  	for(int i=0; i<4;++i){
  		temp = this->listeFace[i].changeLine(temp,2);
  	}
  }

  if(this->listeFace[WHITE].getFace()=="wwwwwwwww" && this->listeFace[YELLOW].getFace()=="yyyyyyyyy" && this->listeFace[RED].getFace()=="rrrrrrrrr" && 
  this->listeFace[BLUE].getFace()=="bbbbbbbbb" && this->listeFace[ORANGE].getFace()=="ooooooooo" && this->listeFace[GREEN].getFace()=="ggggggggg"){
    std::cout<<"You Win";
  }
}

void Cube::scramble(){
  for (int i=0; i<60; ++i){ //60 rotations aléatoires
    this->rotation(std::rand() % 6); 
  }
}

void Cube::afficherCube(){
  for(int i=0; i<3; ++i){ //affichage top face
    std::cout<<"    "<<this->listeFace[WHITE].getCol(i)[2]<<this->listeFace[WHITE].getCol(i)[1]<<this->listeFace[WHITE].getCol(i)[0]<<std::endl;
  }
  for(int i=0; i<3; ++i){ //affichage bague
    std::cout<<this->listeFace[3].getLine(i)<<" "<<this->listeFace[0].getLine(i)<<" "<<this->listeFace[1].getLine(i)<<" "<<this->listeFace[2].getLine(i);
    std::cout<<std::endl;
  }
  for(int i=2; i>=0; --i){ // affichage down face
    std::cout<<"    "<<this->listeFace[5].getCol(i)[2]<<this->listeFace[5].getCol(i)[1]<<this->listeFace[5].getCol(i)[0]<<std::endl;
  }

}
