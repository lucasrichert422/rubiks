#include "Face.hpp"



class Cube{
	enum {RED,BLUE,ORANGE,GREEN,WHITE,YELLOW}; //'r','b','o','g','w','y'
	Face* listeFace;
	int frontFace;
	int topFace;

public:
	Cube();
	~Cube();
	void rotation(int face);
	void afficherCube();
	void scramble();
	int getIntFace(char face);
};
