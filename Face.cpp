#include <iostream>
#include "Face.hpp"
#include <cstring>
using namespace std;

Face::Face(){
	
}

Face::Face(char couleur) {

	this->face = new char[9]; 		// ex "wwwwwwwww" face blanche "012345678"

	for(int i=0;i<9;++i){
		this->face[i]= couleur;
	}

}

Face::~Face() {

}

void Face::Rotation() { // ritation sens horaire

	char* res = new char[9];

	for(int i=0 ;i<9;++i){
		switch (i)
		{
		case 0:
			res[i]=this->face[6];
			break;

		case 1:
			res[i]=this->face[3];
			break;

		case 2:
			res[i]=this->face[0];
			break;

		case 3:
			res[i]=this->face[7];
			break;
		case 4:
			res[i]=this->face[4];
			break;

		case 5:
			res[i]=this->face[1];
			break;

		case 6:
			res[i]=this->face[8];
			break;

		case 7:
			res[i]=this->face[5];
			break;

		case 8:
			res[i]=this->face[2];
			break;

		default:
			break;
		}
	}
	delete this->face; // allocation dynamique suppression de l'ancienne chaine de char
	this->face = res; //allocation dynamique recup nouvelle chaine de char

}

char* Face::getLine(int pos) { // recuperation d'une ligne
	char* res = new char[3];

	for (int i = 0; i < 3; ++i) {
		res[i] = this->face[i + (pos * 3)]; // ex : pos = 1 "0+1*3 1+1*3 2+1*3" = "345"
	}
	
	return res;
}

char* Face::getCol(int pos) { //récuperation d'un colonne
	char* res = new char[3];

	for (int i = 0; i < 3; ++i) {
		res[i] = this->face[i * 3 + pos];// ex : pos = 0 "0+0 3+0 6+0" "036"
	}

	return res;
}

char* Face::changeLine(char* line,int pos){ // échange une ligne avec une autre passée en paramètre et retourner l'ancienne
	char* res = new char[3];
	for(int i=0;i<3;++i){
		res[i]=this->face[3*pos+i]; // récupération ancienne ligne
		this->face[3*pos+i] = line[i]; // changement avec la nouvelle ligne
	}
	delete line;
	return res;
}

char* Face::changeCol(char* col,int pos){ // échange une colonne avec une autre passée en paramètre et retourner l'ancienne
	char* res = new char[3];
	for(int i=0;i<3;++i){
		res[i]=this->face[pos+3*i]; //récupération ancienne colonne 
		this->face[pos+3*i]=col[i]; // échange avec la nouvelle
	}
	delete col;
	return res;
}

char* Face::getFace(){ // récupère la face
	return this->face;
}

void Face::afficherFace(){ // affiche la face 
	for(int i=0;i<9;++i){
		cout << this->face[i];  
		if ((i+1)%3==0){
			cout << "\n";
		}

	}
}
