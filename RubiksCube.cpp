#include <iostream>
#include <cctype>
#include <cstring>
#include "Cube.hpp"

void toUpperStr(char* str){
  for(int i=0; i<strlen(str);++i){
    str[i] = std::toupper(str[i]);
  }
}

void testCube(Cube* cube){
  int testInt[20];
  for(int i=0; i<20; ++i){
    testInt[i] = std::rand()%6;
    cube->rotation(testInt[i]);
    std::cout<<"rotation : "<<testInt[i]<<std::endl;
  }
  std::cout<<"Dans l'autre sens : "<<std::endl;
  for(int i=19;i>=0;--i){
    std::cout<<"rotation : "<<testInt[i]<<std::endl;
    for(int j=0; j<3; ++j){
      cube->rotation(testInt[i]);
    }
  }

  cube->afficherCube();
}

void actions(char * action, Cube* cube){
  if(strlen(action)==1 && action[0]== 'U')
      cube->rotation(cube->getIntFace('U'));
  else if(strlen(action)==2 && action[0]== 'U' && action[1]=='\''){
      cube->rotation(cube->getIntFace('U'));
      cube->rotation(cube->getIntFace('U'));
      cube->rotation(cube->getIntFace('U'));
  }

  else if(strlen(action)==1 && action[0]== 'F')
      cube->rotation(cube->getIntFace('F'));
  else if(strlen(action)==2 && action[0]== 'F' && action[1]=='\''){
      cube->rotation(cube->getIntFace('F'));
      cube->rotation(cube->getIntFace('F'));
      cube->rotation(cube->getIntFace('F'));
  }

  else if(strlen(action)==1 && action[0]== 'R')
      cube->rotation(cube->getIntFace('R'));
  else if(strlen(action)==2 && action[0]== 'R' && action[1]=='\''){
      cube->rotation(cube->getIntFace('R'));
      cube->rotation(cube->getIntFace('R'));
      cube->rotation(cube->getIntFace('R'));
  }
      
  else if(strlen(action)==1 && action[0]== 'L')
      cube->rotation(cube->getIntFace('L'));
  else if(strlen(action)==2 && action[0]== 'L' && action[1]=='\''){
      cube->rotation(cube->getIntFace('L'));
      cube->rotation(cube->getIntFace('L'));
      cube->rotation(cube->getIntFace('L'));
  }

  else if(strlen(action)==1 && action[0]== 'B')
      cube->rotation(cube->getIntFace('B'));
  else if(strlen(action)==2 && action[0]== 'B' && action[1]=='\''){
      cube->rotation(cube->getIntFace('B'));
      cube->rotation(cube->getIntFace('B'));
      cube->rotation(cube->getIntFace('B'));
  }

  else if(strlen(action)==1 && action[0]== 'D')
      cube->rotation(cube->getIntFace('D'));
  else if(strlen(action)==2 && action[0]== 'D' && action[1]=='\''){
      cube->rotation(cube->getIntFace('D'));
      cube->rotation(cube->getIntFace('D'));
      cube->rotation(cube->getIntFace('D'));
  }

  else if(strcmp(action,"HELP") == 0){
      std::cout<<"Type SRAMBLE to scramble the cube"<<std::endl;
      std::cout<<"Type RESET to reset the cube"<<std::endl;
      std::cout<<"Type the normal rotations of a rubik's cube : "<<std::endl;
      std::cout<<"U : up face U' : inverse up face"<<std::endl;
      std::cout<<"F : front face F' : inverse up face"<<std::endl;
      std::cout<<"R : right face R' : inverse front face"<<std::endl;
      std::cout<<"L : left face L' : inverse left face"<<std::endl;
      std::cout<<"B : back face B' : inverse back face"<<std::endl;
      std::cout<<"D : down face D' : inverse down face"<<std::endl;
      std::cout<<"Type QUIT to quit"<<std::endl;
  }

  else if(strcmp(action,"SCRAMBLE") == 0)
      cube->scramble();

  else if(strcmp(action,"RESET") == 0){
      delete cube;
      cube = new Cube;
  }
  
  else
    std::cout << "Enter a correct action (if you don't know what to do type : help)" << std::endl;
  
}

int main(int argc, char const *argv[]) {
  Cube* cube = new Cube;
  bool start = true;
  char* buffer = new char[1024];

  std::cout << "Enter an action (if you don't know what to do type : help)" << std::endl;
  cube->afficherCube();

  while(start){
   std::cin>>buffer;
   toUpperStr(buffer);
   if(strcmp(buffer,"QUIT") == 0)
      start = false;
    else{
        actions(buffer, cube); 
        cube->afficherCube();
    }
  }

  testCube(cube);
  
  return 0;
}
